# Frequency stabilisation
Modelling the AEI 10m prototype frequency stabilisation control loop.

## Installation
Installation requires [zero](https://git.ligo.org/sean-leavey/zero), which is
not yet available on pip, so it must be installed using the repository.

```bash
pip install git+https://github.com/SeanDS/zero.git@0.5.7
cd /path/to/this/repo
python setup.py -e .
```

## Examples
See the `examples` directory for plots of open loop gains, actuator rms signals
and transfer functions.


## Credits
Sean Leavey
<sean.leavey@ligo.org>
