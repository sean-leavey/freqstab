from setuptools import setup, find_packages

with open("README.md") as readme_file:
    README = readme_file.read()

REQUIREMENTS = [
    "numpy",
    "scipy",
    "matplotlib",
    "control",
    "PyKat >= 1.1.310",
    "progressbar2",
]

setup(
    name="freqstab",
    use_scm_version={
        "write_to": "freqstab/_version.py"
    },
    description="Frequency stabilisation model",
    long_description=README,
    author="Sean Leavey",
    author_email="sean.leavey@ligo.org",
    url="https://git.ligo.org/sean-leavey/frequency-stabilisation",
    packages=find_packages(),
    install_requires=REQUIREMENTS,
    setup_requires=['setuptools_scm'],
    license="GPLv3",
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7"
    ]
)
