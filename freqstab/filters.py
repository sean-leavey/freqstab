import numpy as np
from scipy.signal import zpk2ss
from control import StateSpace

def freqrespc(f, model):
    """Frequency response of model, as complex coefficients."""
    mag_tmp, phase_tmp, _ = model.freqresp(2 * np.pi * f)
    mag = np.atleast_1d(np.squeeze(mag_tmp))
    phase = np.atleast_1d(np.squeeze(phase_tmp))

    return mag * (np.cos(phase) + 1j * np.sin(phase))

def highpass(f_cutoff, gain=1):
    """High pass filter in state-space representation.

    Parameters
    ----------
    f_cutoff : :class:`float`
        Low-frequency cut-off (Hz).
    gain : :class:`float`
        High frequency gain.

    Returns
    -------
    :class:`.StateSpace`
        State space filter representation.
    """
    z = [0]
    p = [-2 * np.pi * f_cutoff]
    k = gain

    return StateSpace(*zpk2ss(z, p, k))

def lowpass(f_cutoff, gain=1):
    """Low pass filter in state-space representation.

    Parameters
    ----------
    f_cutoff : :class:`float`
        High-frequency cut-off (Hz).
    gain : :class:`float`
        DC gain.

    Returns
    -------
    :class:`.StateSpace`
        State space filter representation.
    """
    z = []
    p = [-2 * np.pi * f_cutoff]
    k = -gain * 2 * np.pi * f_cutoff

    return StateSpace(*zpk2ss(z, p, k))

def transint(lf, hf, gain=1):
    """Transistional integrator in state-space representation.

    Parameters
    ----------
    lf : float
        Integration start frequency.
    hf : float
        Integration stop frequency.
    gain : float
        DC gain.

    Returns
    -------
    :class:`.StateSpace`
        State space filter representation.
    """
    z = -2 * np.pi * hf
    p = -2 * np.pi * lf
    k = gain * lf / hf

    return StateSpace(*zpk2ss(z, p, k))

def transdif(lf, hf, gain=1):
    """Transistional differentiator in state-space representation.

    Parameters
    ----------
    lf : float
        Differentiation start frequency.
    hf : float
        Differentiation stop frequency.
    gain : float
        DC gain.

    Returns
    -------
    :class:`.StateSpace`
        State space filter representation.
    """
    z = -2 * np.pi * lf
    p = -2 * np.pi * hf
    k = gain * hf / lf

    return StateSpace(*zpk2ss(z, p, k))

def sculte(f_peak, q_peak, f_notch, q_notch, gain=1):
    """Scultete filter in state-space representation.

    For making resonant gain filters, set f_peak == f_notch, choose (q_peak / q_notch)
    to match the desired height, and q_peak to define the resulting filter's q-factor
    (central frequency / FWHM).

    Parameters
    ----------
    f_peak : :class:`float`
        Frequency cut-off (Hz).
    q_peak : :class:`float`
        Q-factor at cutoff frequency.
    f_notch : :class:`float`
        Notch frequency (Hz).
    q_notch : :class:`float`
        Q-factor at notch frequency.
    gain : :class:`float`
        DC gain.

    Returns
    -------
    :class:`.StateSpace`
        State space filter representation.
    """
    z = np.pi * f_notch * (-1 / q_notch + 1j * np.sqrt(4 - 1 / (q_notch ** 2)))
    z = [np.conj(z), z]
    p = np.pi * f_peak * (-1 / q_peak + 1j * np.sqrt(4 - 1 / (q_peak ** 2)))
    p = [np.conj(p), p]
    k = gain * (f_peak / f_notch) ** 2

    return StateSpace(*zpk2ss(z, p, k))

def notch(f_notch, q_notch, gain=1):
    """Notch filter in state-space representation.

    Parameters
    ----------
    f_notch : :class:`float`
        Notch frequency (Hz).
    q_notch : :class:`float`
        Q-factor at notch frequency.
    gain : :class:`float`
        DC gain.

    Returns
    -------
    :class:`.StateSpace`
        State space filter representation.
    """
    z = np.pi * f_notch * (-1 / q_notch + 1j * np.sqrt(4 - 1 / (q_notch ** 2)))
    z = [np.conj(z), z]
    p = [-2 * np.pi * f_notch] * 2
    k = gain

    return StateSpace(*zpk2ss(z, p, k))
