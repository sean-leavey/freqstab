import logging
import locale

def add_log_handler(logger, handler, format_str="%(name)-20s - %(levelname)-8s - %(message)s"):
    """Add log handler to specified logger."""
    handler.setFormatter(logging.Formatter(format_str))
    logger.addHandler(handler)

def set_log_verbosity(level, logger=None):
    """Enable logging to stdout with a certain level"""
    if logger is None:
        logger = LOGGER
    logger.setLevel(level)

# Suppress warnings when code does not include a handler. This is the catch-all root logger.
logging.getLogger().addHandler(logging.NullHandler())

# Create base logger for the package.
LOGGER = logging.getLogger(__name__)
add_log_handler(LOGGER, logging.StreamHandler())

# Set default locale (required for number formatting in log warnings).
locale.setlocale(locale.LC_ALL, "")
