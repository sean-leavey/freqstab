import abc
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker


class BasePlotter(metaclass=abc.ABCMeta):
    def __init__(self, components, title=None, figsize=(8, 6)):
        self.title = title
        self.figsize = figsize
        self.components = components

        # defaults
        self._figure = None

    def figure(self):
        if self._figure is None:
            self._figure = plt.figure(figsize=self.figsize)
        return self._figure

    @staticmethod
    def show():
        plt.show()

    def save(self, *args, **kwargs):
        plt.savefig(*args, **kwargs)


class BodePlotter(BasePlotter):
    def __init__(self, components, mag_scale="db", phase_scale="deg", xlim=None, mag_ylim=None,
                 phase_ylim=None, **kwargs):
        super().__init__(components, **kwargs)

        self.mag_scale = mag_scale
        self.phase_scale = phase_scale
        self.xlim = xlim
        self.mag_ylim = mag_ylim
        self.phase_ylim = phase_ylim

        # running list of components called via plot()
        self.plotted_components = []

        # create plot
        fig = self.figure()
        self.ax1 = fig.add_subplot(211)
        self.ax2 = fig.add_subplot(212, sharex=self.ax1)

        for component in components:
            self._plot(component)

    def complex_magnitude(self, component):
        return component.data

    def scaled_magnitude(self, component, scale="db"):
        scale = scale.lower()

        if scale in ["db", "decibel"]:
            # convert from magnitude to dB
            return 20 * np.log10(np.abs(self.complex_magnitude(component)))
        elif scale in ["abs", "absolute"]:
            return np.abs(self.complex_magnitude(component))
        raise ValueError("don't know what to do with magnitude scale %s" % scale)

    def scaled_phase(self, component, scale="deg"):
        scale = scale.lower()

        if scale in ["deg", "degree", "degrees"]:
            # convert to degrees
            return np.angle(self.complex_magnitude(component)) * 180 / np.pi
        elif scale in ["rad", "radian", "radians"]:
            # convert to radians
            return np.angle(self.complex_magnitude(component))
        else:
            raise ValueError("don't know what to do with phase scale %s" % scale)

    @property
    def mag_title(self):
        return self.title

    @property
    def input_unit(self):
        # use first plotted component
        if len(self.plotted_components):
            return self.plotted_components[0].input_unit

        # default
        return None

    @property
    def output_unit(self):
        # use first plotted component
        if len(self.plotted_components):
            return self.plotted_components[0].output_unit

        # default
        return None

    def _plot(self, component):
        # check units
        if (self.input_unit is not None and component.input_unit is not None
            and self.input_unit != component.input_unit):
            raise ValueError(f"specified component input unit ({component.input_unit}) is "
                             f"incompatible this plot's ({self.input_unit})")
        if (self.output_unit is not None and component.output_unit is not None
            and self.output_unit != component.output_unit):
            raise ValueError(f"specified component output unit ({component.output_unit}) is "
                             f"incompatible this plot's ({self.output_unit})")

        self._plot_magnitude(component)
        self._plot_phase(component)

        self.plotted_components.append(component)

        # format labels etc.
        self._plot_meta()

    def _plot_magnitude(self, component):
        if self.mag_scale in ["db", "decibel"]:
            method = self.ax1.semilogx
        else:
            method = self.ax1.loglog

        magnitude = self.scaled_magnitude(component)
        method(component.f, magnitude, label=str(component))

    def _plot_phase(self, component):
        self.ax2.semilogx(component.f, self.scaled_phase(component), label=str(component))

    def _plot_meta(self):
        # set labels
        self.ax1.set_ylabel("Magnitude (%s)" % self.mag_label)
        self.ax1.grid(True)
        self.ax2.set_xlabel("Frequency (Hz)")
        self.ax2.set_ylabel("Phase (%s)" % self.phase_label)
        self.ax2.grid(True)
        self.ax1.set_title(self.mag_title)

        # set legend
        self.ax1.legend([str(component) for component in self.plotted_components])

        # set limits
        self.ax1.set_xlim(self.xlim)
        self.ax1.set_ylim(self.mag_ylim)
        self.ax2.set_xlim(self.xlim)
        self.ax2.set_ylim(self.phase_ylim)

        if self.mag_scale in ["db", "decibel"]:
            # set tick spacing
            locator = ticker.MultipleLocator(base=20)
            self.ax1.yaxis.set_major_locator(locator)

        plt.tight_layout()

    @property
    def mag_label(self):
        prefix = self.mag_scale.lower()

        if prefix == "db":
            prefix = "dB "
        else:
            prefix = ""

        return "%s%s/%s" % (prefix, self.output_unit, self.input_unit)

    @property
    def phase_label(self):
        units = self.phase_scale.lower()

        if units in ["deg", "degree", "degrees"]:
            return "°"
        elif units in ["rad", "radian", "radians"]:
            # convert to radians
            return "rad"

        raise ValueError("don't know what to do with unit %s" % units)


class SpectralDensityPlotter(BasePlotter):
    def __init__(self, spectra, xlim=None, ylim=None, **kwargs):
        super().__init__(spectra, **kwargs)
        self.xlim = xlim
        self.ylim = ylim

        # create plot
        fig = self.figure()
        self.ax = fig.gca()

        # running list of spectra called via plot()
        self.plotted_spectra = []

        for spectrum in spectra:
            self._plot(spectrum)

    @property
    def unit(self):
        # use first plotted component
        if len(self.plotted_spectra):
            return self.plotted_spectra[0].unit

        # default
        return None

    def _plot(self, spectrum):
        # check units
        if (self.unit is not None and spectrum.unit is not None
            and self.unit != spectrum.unit):
            raise ValueError(f"specified spectrum unit ({spectrum.unit}) has incompatible "
                             f"units with existing plots ({self.unit}")

        self.ax.loglog(spectrum.f, spectrum.data, label=str(spectrum))

        self.plotted_spectra.append(spectrum)

        # format labels etc.
        self._plot_meta()

    def _plot_meta(self):
        # set labels
        self.ax.set_ylabel("Noise (%s/sqrt(Hz))" % self.unit)
        self.ax.set_xlabel("Frequency (Hz)")
        self.ax.grid(True)
        self.ax.set_title(self.title)

        # set legend
        self.ax.legend([str(spectrum) for spectrum in self.plotted_spectra])

        # set limits
        self.ax.set_xlim(self.xlim)
        self.ax.set_ylim(self.ylim)

        plt.tight_layout()
