import numpy as np
import scipy.constants

from .components import Noise

# Reduced Planck's constant
HBAR = 1.054572e-34

# speed of light
C0 = scipy.constants.c

# laser wavelength
LAMBDA0 = 1064e-9

# reference cavity round trip length
REFCAV_LENGTH_RT = 2 * 10.44 + 0.32
# make REFCAV_LENGTH_RT an exact multiple of wavelengths
REFCAV_LENGTH_RT = REFCAV_LENGTH_RT - np.mod(REFCAV_LENGTH_RT, LAMBDA0)

# SQL cavity length
SQL_ARM_LENGTH = 10.8

# SQL cavity length mismatch (i.e. the Schnupp asymmetry)
SQL_ARM_LENGTH_MISMATCH = 10e-12

# SQL differential arm cavity loss (epsilon)
SQL_ARM_LOSS_MISMATCH = 15e-6

# SQL relative finesse mismatch
SQL_FINESSE_MISMATCH = 0.01

# SQL test mass transmissivities (power)
SQL_ITM_TRANS = 8230e-6
SQL_ETM_TRANS = 20.6e-6

# SQL test mass mass
SQL_TM_MASS = 0.1

## Derived parameters

# laser frequency
F_CARRIER = C0 / LAMBDA0

# Test mass amplitude reflectivities.
SQL_ITM_REFL_AMP = np.sqrt(1 - SQL_ITM_TRANS)
SQL_ETM_REFL_AMP = np.sqrt(1 - SQL_ETM_TRANS)

# SQL loss mismatch per round trip
SQL_ARM_LOSS_MISMATCH_RT = 2 * SQL_ARM_LOSS_MISMATCH / SQL_ITM_TRANS

# SQL arm cavity pole frequency
SQL_ARM_CAVITY_POLE = C0 / (2 * np.pi * SQL_ARM_LENGTH) * np.arcsin((1 - SQL_ITM_REFL_AMP * SQL_ETM_REFL_AMP) /
                            (2 * np.sqrt(SQL_ITM_REFL_AMP * SQL_ETM_REFL_AMP)))

def free_mass_sql(f):
    """Free mass SQL, m/sqrt(Hz)"""
    return np.sqrt(8 * HBAR / (SQL_TM_MASS * (2 * np.pi * f) ** 2))

def sql_noise(f):
    """SQL as a noise object"""
    return Noise("SQL", f, free_mass_sql(f), "m")

def refcav_freq_noise_in_sql_ifo_m(f, refcav_noise, name="SQL noise"):
    """Returns SQL interferometer displacement noise due to reference cavity frequency noise.

    Parameters same as those used in SQLIFO.m as of 2019-01-30, except 10.8 m arm
    length used instead of 10 m.

    Parameters
    ----------
    f               list or :class:`np.ndarray`
                    Frequency vector.
    refcav_noise    list or :class:`np.array`
                    Reference cavity frequency noise (Hz/sqrt(Hz)), as seen by the photodetector.
    name            str (optional)
                    Name to give resulting noise.
    """
    if refcav_noise.unit != "Hz":
        raise ValueError("specified refcav noise unit must be Hz/sqrt(Hz)")

    # angular frequency
    omega = 2 * np.pi * f

    # arm cavity pole
    gamma = 2 * np.pi * SQL_ARM_CAVITY_POLE

    #
    s_c = -1j * omega / gamma

    # optical angular frequency
    omega0 = 2 * np.pi * F_CARRIER

    TF_v = (np.pi / (np.sqrt(2) * omega)) * \
           ((omega0 * SQL_ARM_LENGTH_MISMATCH) / (gamma * SQL_ARM_LENGTH)) * \
           ((2 * s_c) / (1 + s_c) ** 2) * \
           (SQL_ARM_LOSS_MISMATCH_RT + (2 * SQL_FINESSE_MISMATCH))

    TF_x = 2 * np.sqrt(2) * omega0 ** 2 * SQL_ARM_LENGTH_MISMATCH \
           / (gamma ** 2 * SQL_ARM_LENGTH ** 2 * (1 + s_c))

    S_freq = (TF_v / TF_x) ** 2 * refcav_noise.data ** 2

    sql_noise = np.sqrt(np.abs(S_freq))

    return Noise(name, f, sql_noise, "m")
