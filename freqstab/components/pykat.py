import logging
from .component import ResponseComponent
from ..external import Pounce

LOGGER = logging.getLogger(__name__)


class PykatTransferFunction(ResponseComponent):
    """Provides a Pykat output as a transfer function"""
    def __init__(self, name, script_path, f=None, **kwargs):
        # Run Finesse script.
        runner = Pounce(kat_file=script_path, override_f=f)
        LOGGER.info(f"Simulating {name} with Pykat")
        data = runner.run()

        # frequency vector
        f = data[:, 0]

        # extract magnitude and phase
        magnitude = data[:, 1]
        phase = data[:, 2]

        super().__init__(name, f, magnitude, phase=phase, mag_units="absolute",
                         phase_units="degree", **kwargs)
