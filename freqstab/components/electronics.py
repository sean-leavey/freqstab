from functools import reduce
import numpy as np

from .component import ResponseComponent
from ..filters import freqrespc, lowpass


class Photodiode(ResponseComponent):
    """Maps optical power to voltage"""
    def __init__(self, name, f, responsivity, transimpedance, poles=[], **kwargs):
        """Photodiode"""
        # Convert from W -> A (via responsivity), then A -> V (via transimpedance).
        magnitude = responsivity * transimpedance

        if poles:
            # Apply op-amp poles.
            oppoles = reduce(lambda x, y: x * y, [lowpass(f_cutoff=f) for f in poles])
            opresp = freqrespc(f, oppoles)
            magnitude *= opresp

        super().__init__(name, f, magnitude, input_unit="W", output_unit="V", **kwargs)
