import numpy as np
from .component import ResponseComponent


class HzToM(ResponseComponent):
    def __init__(self, name, f, f0, L_rt, **kwargs):
        # df/f = dL/L
        magnitude = L_rt / f0 * np.ones(f.shape)

        super().__init__(name, f, magnitude, input_unit="Hz", output_unit="m", **kwargs)
