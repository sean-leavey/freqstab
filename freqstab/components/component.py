import os
import abc
import operator
import functools
import numbers
import numpy as np
from ..filters import freqrespc


class FrequencyDataObject(metaclass=abc.ABCMeta):
    def __init__(self, name, f, data):
        self.name = name
        self.f = f
        self.data = data

    def __str__(self):
        return self.name


class ResponseComponent(FrequencyDataObject):
    def __init__(self, name, f, magnitude, phase=None, mag_units="complex",
                 phase_units=None, input_unit=None, output_unit=None):
        mag_units = mag_units.lower()

        if mag_units == "complex":
            # complex magnitude contains magnitude and phase already
            phase = 0
        else:
            if magnitude is None:
                raise ValueError("magnitude must be specified")

            if mag_units in ["abs", "absolute"]:
                # do nothing
                pass
            elif mag_units in ["db", "decibel"]:
                # convert dB to magnitude
                magnitude = (10 ** (magnitude / 20))
            else:
                raise ValueError("don't know what to do with magnitude type %s" % mag_units)

            # check dimensions
            if f.shape != magnitude.shape:
                raise ValueError("f and magnitude vector shapes don't match")

            if phase is not None:
                phase_units = phase_units.lower()

                if phase_units in ["deg", "degree", "degrees"]:
                    # convert to radians
                    phase = np.radians(phase)
                elif phase_units in ["rad", "radian", "radians"]:
                    # do nothing
                    pass
                else:
                    raise ValueError("don't know what to do with phase type %s" % phase_units)

                # check dimensions
                if f.shape != phase.shape:
                    raise ValueError("f and phase vector shapes don't match")
            else:
                phase = 0

                if phase_units is not None:
                    raise ValueError("phase units were specified but not phase vector")

                phase_units = None

        # combine absolute magnitude and phase in radians
        data = magnitude * (np.cos(phase) + 1j * np.sin(phase))

        super().__init__(name, f, data)

        self.input_unit = input_unit
        self.output_unit = output_unit

    def __str__(self):
        inunit = self.input_unit
        outunit = self.output_unit

        if inunit is None:
            inunit = "?"
        if outunit is None:
            outunit = "?"

        name = super().__str__()

        return f"{name} ({outunit}/{inunit})"

    @property
    def has_dimensions(self):
        return (self.input_unit is not None and self.output_unit is not None and
                self.input_unit != self.output_unit)

    def compatible_dimensions(self, other):
        if not self.has_dimensions or not other.has_dimensions:
            return True

        return self.output_unit == other.input_unit

    def __mul__(self, other):
        """Multiply this component by the specified one, returning a new component

        This component is the left hand side, other is the right.
        """
        # right hand side data
        if isinstance(other, ResponseComponent):
            data_r = other.data

            # check frequencies
            if np.all(self.f != other.f):
                raise ValueError(f"frequency vectors of {self} and {other} are incompatible")
        elif isinstance(other, numbers.Number):
            data_r = complex(other)
        else:
            raise ValueError(f"don't know how to multiply {self} with {other}")

        if hasattr(other, 'has_dimensions'):
            # check units
            if not self.compatible_dimensions(other):
                raise ValueError(f"dimensions of {self} ({self.output_unit}) and "
                                 f"{other} ({other.input_unit}) are incompatible")

            output_unit = other.output_unit
        else:
            # assume other is a scalar
            output_unit = self.output_unit

        # combined data
        combined_data = self.data * data_r

        # new name
        name = "%s * %s" % (self, other)

        return ResponseComponent(name, self.f, combined_data, input_unit=self.input_unit,
                                 output_unit=output_unit)

    def __rmul__(self, other):
        """Reflective multiply, i.e. A * B where B is this object"""
        try:
            other = complex(other)
        except TypeError:
            # not possible to multiply
            return NotImplemented

        return (self.flip() * other).flip()

    def __truediv__(self, other):
        """Divide this response by the specified one, returning a new response

        This response is the left hand side, other is the right.
        """
        try:
            other_inverted = other.invert()
        except AttributeError:
            # assume other is a scalar
            other_inverted = 1 / other

        return self * other_inverted

    def __rtruediv__(self, other):
        """Reflective division, i.e. A / B where B is this object"""
        try:
            other = complex(other)
        except TypeError:
            # not possible to multiply
            return NotImplemented

        return other * self.invert()

    def __add__(self, other):
        """Add the specified response to this one, returning a new response

        This response is the left hand side, other is the right.
        """
        # right hand side data
        if isinstance(other, ResponseComponent):
            data_r = other.data

            # check frequencies
            if np.all(self.f != other.f):
                raise ValueError(f"frequency vectors of {self} and {other} are incompatible")
        elif isinstance(other, numbers.Number):
            data_r = complex(other)
        else:
            raise ValueError(f"don't know how to add {self} to {other}")

        if isinstance(other, ResponseComponent):
            # check units
            if not self.compatible_dimensions(other):
                raise ValueError(f"dimensions of {self} and {other} are incompatible")
        elif isinstance(other, numbers.Number):
            # assume other is a scalar
            pass
        else:
            raise ValueError(f"don't know how to add {self} to {other}")

        # combined response
        response = self.data + data_r

        return ResponseComponent(self.name, self.f, response, input_unit=self.input_unit,
                                 output_unit=self.output_unit)

    def __sub__(self, other):
        """Subtract the specified response from this one, returning a new response

        This response is the left hand side, the other is the right.
        """
        return self + other.negate()

    def __radd__(self, other):
        # A + B == B + A
        return self + other

    def __rsub__(self, other):
        """Reflective subtraction, i.e. A - B where B is this object"""
        return other + self.negate()

    def flip(self):
        # return self but with input/output units swapped
        return ResponseComponent(self.name, self.f, self.data, input_unit=self.output_unit,
                                 output_unit=self.input_unit)

    def invert(self):
        # return self but with inverted magnitude and swapped units
        name = "1 / %s" % self

        return ResponseComponent(name, self.f, 1 / self.data, input_unit=self.output_unit,
                                 output_unit=self.input_unit)

    def negate(self):
        name = "-%s" % self

        return ResponseComponent(name, self.f, -self.data, input_unit=self.input_unit,
                                 output_unit=self.output_unit)


class StateSpaceComponent(ResponseComponent):
    def __init__(self, name, f, system, **kwargs):
        # complex magnitude
        magnitude = freqrespc(f, system)

        super().__init__(name, f, magnitude, **kwargs)


class Noise(FrequencyDataObject):
    def __init__(self, name, f, spectrum, unit):
        super().__init__(name, f, spectrum)
        self.unit = unit

    def compatible_dimensions(self, other):
        if not other.has_dimensions:
            return True

        return self.unit == other.input_unit

    def __mul__(self, other):
        """Multiply this noise component by a response, returning a new noise component.

        This component is the left hand side, other is the right.
        """
        # right hand side data, absolute units
        data_r = np.absolute(other.data)

        if not hasattr(other, 'input_unit'):
            # not a response
            raise ValueError(f"cannot multiply {self.__class__.__name__} by {other.__class__.__name__}")

        output_unit = other.output_unit
        # check units
        if not self.compatible_dimensions(other):
            raise ValueError(f"units of {self.__class__.__name__} ({self.unit}) "
                             f"and {other.__class__.__name__} ({other.input_unit}) "
                             "are not compatible")

        # combined data
        combined_data = self.data * data_r

        # new name
        name = "%s * %s" % (self, other)

        return Noise(name, self.f, combined_data, output_unit)

    def __truediv__(self, other):
        """Divide this noise by a response, returning a new noise."""
        try:
            other_inverted = other.invert()
        except AttributeError:
            # assume other is a scalar
            other_inverted = 1 / other

        return self * other_inverted

    def cumulative_rms(self):
        # empty results vector
        noise_rms = np.zeros_like(self.f)

        # set the rms for the final value equal to the spectral value
        noise_rms[-1] = np.absolute(self.data[-1])

        # loop over each frequency, summing the rms
        for i in range(len(self.f) - 2, -1, -1):
            # difference in frequency between this bin and the last
            df = self.f[i+1] - self.f[i]

            # rms value for this frequency upwards
            noise_rms[i] = np.sqrt(noise_rms[i+1] ** 2 + df * np.absolute(self.data[i]) ** 2)

        return noise_rms

    def rms(self, band):
        cumulative_rms = self.cumulative_rms()
        return cumulative_rms[np.where(self.f == band)]
