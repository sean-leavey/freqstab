"""Pykat loop tools

Sean Leavey
<sean.leavey@ligo.org>
"""

import sys
import logging
import numpy as np
import progressbar
from pykat import finesse

LOGGER = logging.getLogger("external")

class Pounce:
    """Runs a kat object, optionally with a new x-axis, and returns its results as a matrix

    Supports `finesse.pykat.__init__` parameters.

    Parameters
    ----------
    override_f : iterable
        frequency axis to simulate
    progress : bool
        print simulation progress
    kat_verbose : bool
        kat verbosity
    """
    def __init__(self, override_f=None, progress=True, kat_verbose=False, **kat_args):
        if override_f is not None:
            override_f = np.array(override_f)

        self.override_f = list(override_f)
        self.progress = progress
        self.kat_verbose = kat_verbose
        self.kat_args = kat_args

    @property
    def kat_object(self):
        """Get kat object"""

        # create pykat object
        kat = finesse.kat(**self.kat_args)
        kat.verbose = self.kat_verbose

        # play with things
        #LOGGER.warning("refcav pykat model has overrides")
        #kat.mrc1.T = 8e-6 # 2e-6
        #kat.input.P = 0.120 # 0.1

        return kat

    def run(self):
        """Run simulation, looping if necessary"""

        if self.override_f is not None:
            # custom frequency vector
            return self._run_multi()

        return self._run_single()

    def _run_single(self):
        out = self.kat_object.run()

        return self.data_array(out)

    def _run_multi(self):
        data = None
        has_data = False

        # number of frequencies
        nfreq = len(self.override_f)

        if self.progress:
            # update rate
            update = nfreq // 100
            if update == 0:
                update = 1

            # create frequency generator that updates progress
            frequencies = print_progress(self.override_f, nfreq, update=update)
        else:
            frequencies = self.override_f

        # custom frequencies
        for i, f in enumerate(frequencies):
            kat = self.kat_object

            kat.noxaxis = True
            kat.signals.f = f

            # run Finesse via Pykat
            out = kat.run()

            # create 1D data array
            this_data = self.data_array(out, x=f)

            if not has_data:
                # number of rows == number of frequencies
                nrows = nfreq
                # number of columns == 1 (frequencies) + number of outputs
                ncols = this_data.shape[1]

                # create data matrix from expected final shape
                data = np.zeros((nrows, ncols))

                has_data = True

            # add data into its place
            data[i, :] = this_data

        return data

    def data_array(self, kat_output, x=None):
        """Create a data array given a kat output, optionally overriding the x-axis

        x : x-axis to use if noxaxis
        """

        if x:
            # use custom x
            # assume singular
            x = np.array([[float(x)]])
        else:
            # use kat's x, should have shape (xaxis length,)
            x = np.array(kat_output.x)

            # convert x (1D array) into 2D column vector
            x = np.expand_dims(x, axis=1)

        # get y-axis from kat, should have shape (xaxis length, number of outputs)
        y = kat_output.y

        return np.hstack((x, y))


def print_progress(sequence, total, update=100000, stream=sys.stdout):
    """Print progress of generator with known length

    :param sequence: sequence to report iteration progress for
    :type sequence: Sequence[Any]
    :param total: number of items generator will produce
    :type total: int
    :param update: number of items to yield before next updating display
    :type update: int
    :param stream: output stream
    :type stream: :class:`io.IOBase`
    :return: input sequence
    :rtype: Generator[Any]
    """

    # set up progress bar
    pbar = progressbar.ProgressBar(widgets=['Calculating: ',
                                            progressbar.Percentage(),
                                            progressbar.Bar(),
                                            progressbar.ETA()],
                                   max_value=100, fd=stream).start()

    count = 0

    if update <= 0:
        raise ValueError("update must be > 0")

    for item in sequence:
        count += 1

        if count % update == 0:
            pbar.update(100 * count // total)

        yield item

    # make sure bar finishes at 100
    pbar.update(100)

    # newline before next text
    print(file=stream)


if __name__ == "__main__":
    # triangular cavity transfer function example
    kat_code = """
    const mod_frq 8M
    l laser 0.1 0.0 0.0 n1
    s s1 1.0 n1 n2
    mod eom $mod_frq 0.3 2 pm 0.0 n2 n3
    s s4 0.1 n3 n12
    bs1 m1 0.001 0.0 0.0 44.055674 n12 n13 n14 n15
    s s5 10 n14 n16
    bs1 m2 2e-6 0.0 0.0 1.0 n16 n17 n18 n19
    s s6 10 n17 n20
    bs1 m3 0.001 0.0 0.0 -44.0 n20 n21 n22 n23
    s s7 1 n21 n15

    fsig signal m2 1 0
    pd2 pdrefl $mod_frq 349.9300699 $fs n13
    xaxis signal f log 1 10M 1000 # ignored in `Pounce`
    yaxis log abs:deg

    scale meter
    maxtem off
    """

    # our custom frequency vector
    f = [1, 3, 10, 30, 100, 300, 1000, 3000, 10000, 30000, 100000]

    # create runner
    runner = Pounce(kat_code=kat_code, override_f=f)

    # run
    data = runner.run()

    # plot
    import matplotlib.pyplot as plt
    plt.subplot(2, 1, 1)
    plt.loglog(data[:, 0], data[:, 1])
    plt.ylabel('Response (W/m)')
    plt.grid(True)
    plt.subplot(2, 1, 2)
    plt.semilogx(data[:, 0], data[:, 2])
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Phase (°)')
    plt.grid(True)
    plt.show()
