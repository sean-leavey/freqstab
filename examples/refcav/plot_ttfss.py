import os
import numpy as np
import logging
from freqstab.components import ResponseComponent
from freqstab.plotter import BodePlotter
from ttfss import TTFSSPZT, TTFSSEOM

handler = logging.StreamHandler()
LOGGER = logging.getLogger(__name__)
LOGGER.addHandler(handler)
LOGGER.setLevel(logging.INFO)


THIS_DIR = os.path.dirname(os.path.realpath(__file__))
REF_DATA_PZT = os.path.join(THIS_DIR, 'assets', 'ttfss_pzt_boost_enabled.txt')
REF_DATA_EOM = os.path.join(THIS_DIR, 'assets', 'ttfss_eom_boost_enabled.txt')


def ttfss_tf(f, com_gain=0, fast_gain=0):
    """Plot TTFSS transfer functions.

    Parameters
    ----------
    com_gain : float
        Common gain setting (dB).
    fast_gain : float
        Fast gain setting (dB).
    """
    common_gain = 10 ** (com_gain / 20)
    fast_gain = 10 ** (fast_gain / 20)

    ttfss_pzt = TTFSSPZT(f=f, name="PZT (modelled)", common_gain=common_gain,
                         fast_gain=fast_gain)
    ttfss_eom = TTFSSEOM(f=f, name="EOM (modelled)", common_gain=common_gain,
                         fast_gain=fast_gain)

    # Load reference curves.
    ref_data_1 = np.genfromtxt(REF_DATA_PZT)
    ref_data_2 = np.genfromtxt(REF_DATA_EOM)

    # Create reference components.
    ref_1 = ResponseComponent("PZT path (measured)", ref_data_1[:, 0],
                              magnitude=ref_data_1[:, 1], mag_units="db",
                              phase=ref_data_1[:, 2], phase_units="deg",
                              input_unit="V", output_unit="V")
    ref_2 = ResponseComponent("EOM path (measured)", ref_data_2[:, 0],
                              magnitude=ref_data_2[:, 1], mag_units="db",
                              phase=ref_data_2[:, 2], phase_units="deg",
                              input_unit="V", output_unit="V")

    ### Plots
    pzt_plotter = BodePlotter([ttfss_pzt, ref_1], mag_scale="db",
                              title="TTFSS PZT path transfer function")

    # Scale EOM path to measured data.
    #ttfss_eom *= 3

    eom_plotter = BodePlotter([ttfss_eom, ref_2], mag_scale="db",
                              title="TTFSS EOM path transfer function",
                              mag_ylim=[0,100], xlim=[1e2, 1e7])

    pzt_plotter.show()
    eom_plotter.show()
    #eom_plotter.save('ttfss_eom.png')
    #eom_plotter.save('ttfss_eom.pdf')

if __name__ == "__main__":
    f = np.logspace(2, 7, 250)
    ttfss_tf(f, com_gain=0, fast_gain=0)
