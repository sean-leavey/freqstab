"""Reference cavity loop playground.

This isn't a real system, just a simulation of what we can do with different
loop shapes.
"""

import os
import numpy as np
import logging

from scipy.signal import zpk2ss
from control import StateSpace

from freqstab.constants import (C0, F_CARRIER, REFCAV_LENGTH_RT,
                                refcav_freq_noise_in_sql_ifo_m, sql_noise)
from freqstab.components import ResponseComponent, StateSpaceComponent, Noise
from freqstab.components.misc import HzToM
from freqstab.components.pykat import PykatTransferFunction
from freqstab.components.electronics import Photodiode
from freqstab.filters import lowpass, highpass, transint, transdif, sculte, notch
from freqstab.plotter import BodePlotter, SpectralDensityPlotter

from input import MephistoPZT, NewFocus4004, PreModeCleaner
from ttfss import TTFSSPZT, TTFSSEOM

handler = logging.StreamHandler()
logger = logging.getLogger()
logger.addHandler(handler)
logger.setLevel(logging.INFO)


THIS_DIR = os.path.dirname(os.path.realpath(__file__))
REF_CAV_SCRIPT = os.path.join(THIS_DIR, 'assets', 'pd_rf_tf.kat')


if __name__ == "__main__":
    f = np.logspace(0, 7, 151)

    # Laser actuators (Hz / V).
    laser_pzt = MephistoPZT(f=f, freq_per_volt=1.35e6, name="Laser PZT")
    laser_eom = NewFocus4004(f=f, poles=[100e6], name="Laser EOM")

    # Scale factor (m / Hz).
    hz_to_m = HzToM(f=f, f0=F_CARRIER, L_rt=REFCAV_LENGTH_RT, name="Hz to m")

    # PMC (m / m).
    pmc = PreModeCleaner(f=f, cav_pole=300e3, transmissivity=0.953, name="PMC")

    # Reference cavity response to photodiode (W / m).
    ref_cav = PykatTransferFunction("RefCav", REF_CAV_SCRIPT, f=f,
                                    input_unit="m", output_unit="W")

    # Photodiode response (V / W).
    pd = Photodiode(f=f, responsivity=0.72, transimpedance=130,
                    poles=[50e6, 50e6], name="PD")

    # EOM response
    hz_per_v = ResponseComponent("Hz per V", f, -1j * np.pi * f / 210, input_unit="V", output_unit="Hz")

    # Reference cavity laser frequency noise
    freq_noise_hz_refcav = Noise("Free running laser frequency noise", f, 1 * 10 ** 4 / f, "Hz")
    freq_noise_m_refcav = freq_noise_hz_refcav * hz_to_m

    # SQL
    sql = sql_noise(f)

    ### Filter frequency response.
    ## Common path.
    # Reference cavity pole compensation.
    common_filt = transdif(2e3, 100e6)
    # Pre-mode cleaner cavity pole compensation.
    common_filt *= transdif(300e3, 100e6)

    ## PZT path.
    pzt_filt = common_filt
    # Resonant gain for PZT resonance.
    pzt_filt *= notch(f_notch=118e3, q_notch=2, gain=1)
    # Roll-off after crossover but before PZT resonance.
    pzt_filt *= lowpass(30e3)

    ## EOM path.
    eom_filt = common_filt
    # EOM zero compensation.
    eom_filt *= StateSpace(*zpk2ss([], [0], 10 ** (60 / 20)))

    # Servo blocks.
    pzt_servo = StateSpaceComponent("PZT servo response", f, pzt_filt)
    eom_servo = StateSpaceComponent("EOM servo response", f, eom_filt)

    # The reference cavity plant.
    plant = hz_to_m * pmc * ref_cav * pd
    plant.name = "Plant"

    # Frequency stabilisation system open loop gain.
    open_loop_gain_pzt = plant * pzt_servo * laser_pzt
    open_loop_gain_pzt.name = "Open loop gain (PZT contribution)"
    open_loop_gain_eom = plant * eom_servo * laser_eom
    open_loop_gain_eom.name = "Open loop gain (EOM contribution)"
    open_loop_gain_total = plant * eom_servo * laser_eom - plant * pzt_servo * laser_pzt
    open_loop_gain_total.name = "Open loop gain (total)"

    # Transfer function from laser frequency noise to PZT input.
    pzt_tf = plant * pzt_servo / (1 - open_loop_gain_total)
    pzt_tf.name = "PZT Path"
    # In-loop feedback signal (technically noise) on PZT.
    pzt_fb = freq_noise_hz_refcav * pzt_tf
    pzt_fb.name = "PZT Feedback"

    # Transfer function from laser frequency noise to EOM input.
    eom_tf = plant * eom_servo / (1 - open_loop_gain_total)
    eom_tf.name = "EOM Path"
    # In-loop feedback signal (technically noise) on EOM.
    eom_fb = freq_noise_hz_refcav * eom_tf
    eom_fb.name = "EOM Feedback"

    # Frequency noise suppressed by the loop.
    suppressed_freq_noise_hz_refcav = freq_noise_hz_refcav / open_loop_gain_total
    suppressed_freq_noise_hz_refcav.name = "Suppressed laser frequency noise"
    suppressed_freq_noise_m_refcav = suppressed_freq_noise_hz_refcav * hz_to_m

    # Displacement equivalent suppressed noise in SQL interferometer.
    suppressed_freq_noise_m_sql = refcav_freq_noise_in_sql_ifo_m(f, suppressed_freq_noise_hz_refcav, "Frequency noise")

    # Print actuator rms voltages.
    print("PZT RMS: %e V" % pzt_fb.rms(1))
    print("EOM RMS: %e V" % eom_fb.rms(1))

    ### Plots
    actuator_tfs = BodePlotter([pzt_tf, eom_tf], mag_scale="db", title="Laser frequency to actuator transfer functions")
    #actuator_tfs.save('laser-actuator-tfs.pdf')

    # Servos.
    servo_tfs = BodePlotter([pzt_servo, eom_servo], title="Servos")

    olg_plot = BodePlotter([open_loop_gain_pzt, open_loop_gain_eom,
                            open_loop_gain_total],
                            mag_scale="db", title="Open loop gain")

    actuator_plt = SpectralDensityPlotter([pzt_fb, eom_fb],
                                          title="In-loop actuator signals")
    #actuator_plt.save("laser-actuator-noise.pdf")

    refcav_freq_noise_hz_plt = SpectralDensityPlotter([freq_noise_hz_refcav, suppressed_freq_noise_hz_refcav],
                                                      title="Reference cavity frequency noise")

    refcav_freq_noise_m_plt = SpectralDensityPlotter([freq_noise_m_refcav, suppressed_freq_noise_m_refcav],
                                                     title="Reference cavity frequency noise")

    sql_disp_noise_plt = SpectralDensityPlotter([suppressed_freq_noise_m_sql, sql],
                                                 title="SQL interferometer frequency noise")

    BodePlotter.show()
