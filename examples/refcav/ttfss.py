import os
import logging
import numpy as np
from zero.liso import LisoInputParser
from zero.analysis import AcSignalAnalysis
from zero.config import OpAmpLibrary
from freqstab.components import ResponseComponent

LOGGER = logging.getLogger(__name__)
LIBRARY = OpAmpLibrary()

# Add PA98 to library.
pa98data = {
    "a0": 3e5, # 110 dB
    "gbw": 7e6, # cc = 68p
    "poles": [7e6], # cc = 68p
    "vnoise": 4e-9,
    "vcorner": 2e3,
    "inoise": 1e-15, # guess
    "icorner": 1000, # guess
    "sr": 1000e6
}

LIBRARY.add_data("PA98", pa98data)

# Add AD847 to library.
ad847data = {
    "a0": 5.5e3,
    "gbw": 50e6,
    "vnoise": 15e-9,
    "vcorner": 100,
    "inoise": 1.5e-12, # guess
    "icorner": 100, # guess
    "sr": 300e6
}

LIBRARY.add_data("AD847", ad847data)

THIS_DIR = os.path.dirname(os.path.realpath(__file__))
SCRIPT_PATH = os.path.join(THIS_DIR, 'assets', 'ttfss-2018-06.fil')


class TTFSSTF(ResponseComponent):
    def __init__(self, name, f, output_node, common_gain=1, fast_gain=1, **kwargs):
        circuit = self.get_circuit(common_gain=common_gain, fast_gain=fast_gain)

        # create circuit anaylsis
        analysis = AcSignalAnalysis(circuit=circuit, frequencies=f)

        # Simulate.
        LOGGER.info(f"Simulating {name} response")
        analysis.calculate()

        # get transfer function
        tfs = analysis.solution.filter_tfs(sinks=[output_node])

        if len(tfs) != 1:
            raise Exception("can't handle anything other than a single transfer function")

        tf = tfs[0]
        magnitude = np.abs(tf.series.y)
        phase = np.angle(tf.series.y) * 180 / np.pi

        super().__init__(name, f, magnitude, phase=phase, mag_units="absolute",
                         phase_units="degree", input_unit="V", output_unit="V",
                         **kwargs)

    def get_circuit(self, common_gain=1, fast_gain=1):
        # parse LISO script
        parser = LisoInputParser()
        parser.parse(path=SCRIPT_PATH)

        # get parsed circuit
        circuit = parser.circuit

        # add voltage input at test point 1
        circuit.add_input(input_type="voltage", node="nTP1")

        # set common gain
        circuit["U2B"].a0 = common_gain

        # set fast gain
        circuit["U2A"].a0 = fast_gain

        return circuit


class TTFSSPZT(TTFSSTF):
    def __init__(self, name, f, **kwargs):
        super().__init__(name, f, output_node="nFAST_OUT", **kwargs)


class TTFSSEOM(TTFSSTF):
    def __init__(self, name, f, **kwargs):
        super().__init__(name, f, output_node="nPC_OUT", **kwargs)
