import os
import numpy as np
import logging

from freqstab.constants import C0, F_CARRIER, REFCAV_LENGTH_RT
from freqstab.components.component import ResponseComponent
from freqstab.components.misc import HzToM
from freqstab.components.pykat import PykatTransferFunction
from freqstab.components.electronics import Photodiode
from freqstab.plotter import BodePlotter

from input import MephistoPZT, NewFocus4004, PreModeCleaner
from ttfss import TTFSSPZT, TTFSSEOM

handler = logging.StreamHandler()
logger = logging.getLogger()
logger.addHandler(handler)
logger.setLevel(logging.INFO)

THIS_DIR = os.path.dirname(os.path.realpath(__file__))
REF_CAV_SCRIPT = os.path.join(THIS_DIR, 'assets', 'pd_rf_tf.kat')


def reference_cavity_olg(f, com_gain_db, fast_gain_db):
    common_gain = 10 ** (com_gain_db / 20)
    fast_gain = 10 ** (fast_gain_db / 20)

    ttfss_pzt = TTFSSPZT(f=f, name="TTFSS PZT FB", common_gain=common_gain)
    ttfss_eom = TTFSSEOM(f=f, name="TTFSS EOM FB", common_gain=common_gain, fast_gain=fast_gain)
    laser_pzt = MephistoPZT(f=f, freq_per_volt=1.35e6, name="Laser PZT")
    laser_eom = NewFocus4004(f=f, poles=[100e6], name="Laser EOM")
    #rf_summing = RFSummingBox(f=f, name="RF summing box")
    hz_to_m = HzToM(f=f, f0=F_CARRIER, L_rt=REFCAV_LENGTH_RT, name="Hz to m")
    pmc = PreModeCleaner(f=f, cav_pole=300e3, transmissivity=0.953, name="PMC")
    ref_cav = PykatTransferFunction("RefCav", REF_CAV_SCRIPT, f=f,
                                    input_unit="m", output_unit="W")
    pd = Photodiode(f=f, responsivity=0.72, transimpedance=130,
                    poles=[50e6, 50e6], name="PD")

    # Form loop.
    olg_pzt = ttfss_pzt * laser_pzt * hz_to_m * pmc * ref_cav * pd
    olg_eom = ttfss_eom * laser_eom * hz_to_m * pmc * ref_cav * pd
    olg = (-1 * ttfss_pzt * laser_pzt + ttfss_eom * laser_eom) * hz_to_m * pmc * ref_cav * pd

    olg_pzt.name = "PZT (modelled)"
    olg_eom.name = "EOM (modelled)"
    olg.name = "Total (modelled)"

    # Load reference curves.
    #ref_data_1 = np.genfromtxt(os.path.join(THIS_DIR, 'data/olg-measured/fb2.txt'))
    #ref_data_2 = np.genfromtxt(os.path.join(THIS_DIR, 'data/olg-measured/fb3.txt'))
    #ref_data_3 = np.genfromtxt(os.path.join(THIS_DIR, 'data/olg-measured/fb4.txt'))
    #ref_data_4 = np.genfromtxt(os.path.join(THIS_DIR, 'data/olg-measured/fb5.txt'))
    #ref_data_5 = np.genfromtxt(os.path.join(THIS_DIR, 'data/olg-measured/fb7.txt'))
    #ref_data_6 = np.genfromtxt(os.path.join(THIS_DIR, 'data/olg-measured/estimated.txt'))

    # apply correction as measurements were made with -6dB common gain
    #ref_data_6 *= 6 # not chosen for any particular reason

    # create reference components
    #ref_1 = ResponseComponent("Total (measured)", "V", "V", ref_data_1[:, 0], ref_data_1[:, 1], mag_units="abs")
    #ref_2 = ResponseComponent("Total (measured)", "V", "V", ref_data_2[:, 0], ref_data_2[:, 1], mag_units="abs")
    #ref_3 = ResponseComponent("Total (measured)", "V", "V", ref_data_3[:, 0], ref_data_3[:, 1], mag_units="abs")
    #ref_4 = ResponseComponent("Total (measured)", "V", "V", ref_data_4[:, 0], ref_data_4[:, 1], mag_units="abs")
    #ref_5 = ResponseComponent("Total (measured)", "V", "V", ref_data_5[:, 0], ref_data_5[:, 1], mag_units="abs")
    #ref_6 = ResponseComponent("Total (measured)", "V", "V", ref_data_6[:, 0], ref_data_6[:, 1], mag_units="abs")

    ### Plots
    BodePlotter([olg, olg_pzt, olg_eom], mag_scale="db", title="Reference cavity",
                mag_ylim=[-80, 160])

    #plotter.save('refcav.png')
    #plotter.save('refcav.pdf')

    BodePlotter.show()

if __name__ == "__main__":
    f = np.logspace(0, 7, 100)
    reference_cavity_olg(f, 0, 0)
