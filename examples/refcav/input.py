import functools
import operator
import numpy as np

from freqstab.components import ResponseComponent
from freqstab.filters import freqrespc, lowpass, sculte


class MephistoPZT(ResponseComponent):
    def __init__(self, name, f, freq_per_volt, **kwargs):
        # laser PZT response, 1.35 MHz/V
        magnitude = complex(freq_per_volt) * np.ones(f.shape)

        # add mechanical resonance (guess; look at p3118, note this seems to change slightly)
        # approx. 15 dB == 5.6 in height, Q of about 13
        pzt_pole = sculte(f_peak=118e3, q_peak=13, f_notch=118e3, q_notch=13/5.6, gain=1)
        magnitude *= freqrespc(f, pzt_pole)

        super().__init__(name, f, magnitude, input_unit="V", output_unit="Hz", **kwargs)


class NewFocus4004(ResponseComponent):
    """Maps EOM voltage to frequency change"""
    def __init__(self, name, f, poles, **kwargs):
        # EOM poles
        mech_sys = functools.reduce(operator.mul, [lowpass(f_pole) for f_pole in poles])
        mech_tf = freqrespc(f, mech_sys)

        # voltage for pi radians phase shift (from datasheet: New Focus 4004 broadband modulator)
        v_pi = 210

        v_per_rad = v_pi / np.pi

        # radians per volt
        rad_per_v = 1 / v_per_rad

        # convert phase to frequency
        hz_per_v = -1j * f * rad_per_v

        # multiply by mechanical transfer function
        magnitude = hz_per_v * mech_tf

        super().__init__(name, f, magnitude, input_unit="V", output_unit="Hz", **kwargs)


class RFSummingBox(ResponseComponent):
    """Maps voltage to voltage via the RF summing box

    See logbook p1035.
    """
    def __init__(self, name, f, **kwargs):
        # assume flat at frequencies we care about
        magnitude = 1 * np.ones(f.shape)

        super().__init__(name, f, magnitude, input_unit="V", output_unit="V", **kwargs)


class PreModeCleaner(ResponseComponent):
    """Maps displacement to displacement, with PMC mechanical effects"""
    def __init__(self, name, f, cav_pole, transmissivity, *args, **kwargs):
        # low pass filter
        cavity_sys = lowpass(f_cutoff=cav_pole, gain=transmissivity)
        cavity_tf = freqrespc(f, cavity_sys)

        super().__init__(name, f, cavity_tf, input_unit="m", output_unit="m", **kwargs)
